package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class Tc001_LoginAndLogout extends SeMethods{

	@Test
	public void login() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUsername = locateElement("id", "username");
	type(eleUsername, "DemoSalesManager");
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	WebElement linkCRMSFA = locateElement("linkText","CRM/SFA");
	click(linkCRMSFA);
	WebElement createLead = locateElement("linkText","Create Lead");
	click(createLead);
	WebElement compName = locateElement("id","createLeadForm_companyName");
	type(compName,"TCS");
	WebElement fName = locateElement("id","createLeadForm_firstName");
	type(fName,"Revathy");
	WebElement lName = locateElement("id","createLeadForm_lastName");
	type(lName,"Natarajan");
	WebElement source = locateElement("id","createLeadForm_dataSourceId");
	selectDropDownUsingText(source,"Public Relations");
	WebElement marCamp = locateElement("id","createLeadForm_marketingCampaignId");
	selectDropDownUsingIndex(marCamp,1);
	WebElement phNo = locateElement("id","createLeadForm_primaryPhoneNumber");
	type(phNo,"8056228722");
	WebElement submit = locateElement("class","smallSubmit");
	click(submit);
	
	
	}
	
}







