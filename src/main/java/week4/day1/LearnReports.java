package week4.day1;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {

	public static void main(String[] args) {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./report/report.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
	ExtentTest test = extent.createTest("TC001_Login","Login to leaftaps");
		test.assignAuthor("Revathy");
	test.assignCategory("Smoke");
	test.pass("Enter username successfully");
	test.pass("Enter password successfully");
	//test.pass("Click login button successfully");
	test.fail("click login failed");
	extent.flush();
	
		
		
	}

}
