package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import week4.day1.LearnReport;

public class SeMethods extends LearnReport implements WdMethods {

	RemoteWebDriver driver = null;
	public static int i;
	@Override
	public void startApp(String browser, String url) {
		try {
	
	if (browser.equalsIgnoreCase("chrome")) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	} else if(browser.equalsIgnoreCase("firefox")) {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		driver = new FirefoxDriver();
	}
	driver.get(url);
	driver.manage().window().maximize();
	//System.out.println("The browser "+browser+" launched successfully");
	reportStep("Pass", "The browser "+browser+" launched successfully");
	}catch (WebDriverException e) {
		System.err.println("WebDriverException");
		reportStep("Fail", "Browser not launched");
		throw new RuntimeException();
	}
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
	try {
		switch (locator) {
		case "id": return driver.findElementById(locValue); 
		case "class": return  driver.findElementByClassName(locValue);
		case "linkText": return driver.findElementByLinkText(locValue);
		case "name": return driver.findElementByName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		
		}
	} catch (NoSuchElementException e) {
		System.err.println("Element not found");
		//throw new RuntimeException();
	}catch (WebDriverException e) {
		System.err.println("WebDriverException "+e.getMessage());
		//throw new RuntimeException();
	}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
		
		 
	}

	@Override
	public void type(WebElement ele, String data) {
	try {
       ele.sendKeys(data);	
       //System.out.println("The data "+data+" entered successfully");
       reportStep("Pass", "The data "+data+" entered successfully");
	}catch (WebDriverException e) {
		System.err.println("WebDriverException occured");
		reportStep("Fail", "The data "+data+" not entered");
		throw new RuntimeException();
	}finally {
		takeSnap();
	}
	}

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();
		//System.out.println("The element "+ele+ "click successfully");
		reportStep("Pass", "The element "+ele+ "click successfully");
	}catch (WebDriverException e) {

		reportStep("Fail", "The element "+ele+ "click successfully");
	}
		takeSnap();
	}
	public void clickWithOutSnap(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+ "click successfully");
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
	Select sel = new Select(ele);
	sel.selectByVisibleText(value);
	System.out.println("The value "+value+" entered successfully");
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select select = new Select(ele);
		select.selectByIndex(index);
				
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String title = driver.getTitle();
		if(expectedTitle.equals(title)) {
		System.out.println("The title is "+ title);
		return true;
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String txt = ele.getText();
		if(txt.equals(expectedText)) {
			System.out.println("The text is " +txt+ "exact text");
		}
		
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText)) {
			System.out.println("The text" +text+ " partially matched");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void verifySelected(WebElement ele) {
	ele.isSelected();
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
	ele.isDisplayed();
		
	}

	@Override
	public void switchToWindow(int index) {
		Set<String> windowHandles = driver.getWindowHandles();
		List <String> list=new ArrayList<String>();
		list.addAll(windowHandles);
		driver.switchTo().window(list.get(index));
		
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
		
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		
		
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		
	}

	@Override
	public String getAlertText() {
		return driver.switchTo().alert().getText();
		
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest=new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dest);
		}catch (IOException e) {
			e.printStackTrace();
			System.err.println("IO exception occured");
		}
		
		i++;
		
	}

	@Override
	public void closeBrowser() {
		driver.close();
		
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
	}

}
