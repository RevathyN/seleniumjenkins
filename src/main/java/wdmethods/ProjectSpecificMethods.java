package wdmethods;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdmethods.SeMethods;

public class ProjectSpecificMethods extends SeMethods {
	public ChromeDriver driver;
	@Parameters({"browser","url","username","password"})
	@BeforeMethod
	public void login(String browser,String url,String username,String password  ) {
		startApp(browser,url);
		/*driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();*/
		WebElement ele1 = locateElement("id","username");
		type(ele1,username);
		WebElement ele2 = locateElement("id","password");
		type(ele2,password);
		WebElement ele3 = locateElement("class","decorativeSubmit");
		ele3.click();
		WebElement ele4 = locateElement("linkText","CRM/SFA");
		ele4.click();
		
	}
	@AfterMethod
	public void close() {
		closeBrowser();
	}

}
