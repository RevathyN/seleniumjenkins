package week1.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;
import week3.day2.ReadExcel;




public class CreateLeadHW extends ProjectSpecificMethods {
	@BeforeTest
	public void setData() {
		testcaseName="TC01_CreateLead";
		testdesp="Create a new Lead";
		author="Revathy";
		category="Smoke";
	}
	@Test(dataProvider="testData")
public  void CreateAnotherLead(String cname,String fname,String lname) {
		WebElement lnk1 = locateElement("linkText","Create Lead");
		lnk1.click();
		WebElement txt1 = locateElement("id","createLeadForm_companyName");
		type(txt1, cname);
		WebElement txt2 = locateElement("id","createLeadForm_firstName");
		type(txt2,fname);
		WebElement txt3 = locateElement("id","createLeadForm_lastName");
		type(txt3,lname);
		WebElement phno = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(phno,"8056228722");
		WebElement bttn1 = locateElement("class","smallSubmit");
		bttn1.click();
		
	}
			

/*driver.findElementByLinkText("Create Lead").click();
driver.findElementById("createLeadForm_companyName").sendKeys("Revathy");
driver.findElementById("createLeadForm_firstName").sendKeys("First");
driver.findElementById("createLeadForm_lastName").sendKeys("Last");
driver.findElementById("createLeadForm_dataSourceId").sendKeys("CONFERENCE");
driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Car and Driver");
driver.findElementById("createLeadForm_firstNameLocal").sendKeys("LocalFirstName");
driver.findElementById("createLeadForm_lastNameLocal").sendKeys("LocalLastName");
driver.findElementById("createLeadForm_personalTitle").sendKeys("Ms");
driver.findElementById("createLeadForm_generalProfTitle").sendKeys("DR");
driver.findElementById("createLeadForm_departmentName").sendKeys("Test Dept");
driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee");
driver.findElementById("createLeadForm_industryEnumId").sendKeys("Computer Software");
driver.findElementById("createLeadForm_numberEmployees").sendKeys("3000");
driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("S-Corporation");
driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8056228722");
driver.findElementById("createLeadForm_primaryEmail").sendKeys("revathynatarajan@gmail.com");
driver.findElementByClassName("smallSubmit").click();
driver.findElementByLinkText("Logout").click();
*/





	




@DataProvider(name="testData")
public String[][] getData() throws IOException {
	String[][] data = ReadExcel.readExcel();
	   return data;
}


		
		

}




/*public String[][] dynamicData(){
	
	String[][] data=new String[2][3];
	data[0][0]="TestLeaf";
	data[0][1]="Revathy";
	data[0][2]="Natarajan";
	data[1][0]="Temenos";
	data[1][1]="Ramya";
	data[1][2]="Last";
	return data;
}*/

	
