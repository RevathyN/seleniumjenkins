package week1.day4;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class EditLeadHW extends ProjectSpecificMethods{
@Test(groups="regression",dependsOnGroups="smoke")
	public  void EditLead() throws InterruptedException {
		WebElement lnk1 = locateElement("linkText","Create Lead");
		lnk1.click();
		WebElement lnk2 = locateElement("linkText","Find Leads");
		lnk2.click();
		WebElement fname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(fname,"Revathy");
		WebElement bttn1 = locateElement("xpath","//button[text()='Find Leads']");
		bttn1.click();
		Thread.sleep(3000);
		WebElement leadid = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
		
		leadid.click();
		
		verifyTitle("View Lead | opentaps CRM");
		WebElement lnk3 = locateElement("linkText","Edit");
		lnk3.click();
		WebElement comp = locateElement("xpath","(//input[@name='companyName'])[2]");
		comp.clear();
		type(comp,"Temenos123");
		WebElement sub1 = locateElement("xpath","//input[@name='submitButton'][1]");
		sub1.click();
		WebElement compname1 = locateElement("id","viewLead_companyName_sp");
		String text = getText(compname1);
		
								//driver.findElementByLinkText("12395").click();
				//driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();
				/*String title = driver.getTitle();
				if(title.equals("View Lead | opentaps CRM")) {
					System.out.println("The title is " + title);
				}*/
				
/*driver.findElementByLinkText("Edit").click();
driver.findElementByXPath("(//input[@name='companyName'])[2]").clear();
driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("Temenos");
driver.findElementByXPath("//input[@name='submitButton'][1]").click();
String CompName = driver.findElementById("viewLead_companyName_sp").getText();
*/
if(text.contains("Temenos123")) {
	System.out.println("Company Name updated Correctly");
	
}



	}

}
