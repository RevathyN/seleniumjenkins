package week1.day4;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DuplicateLeadHW {
@Test(enabled=false)
	public  void duplicateLead() throws InterruptedException {
		ChromeDriver driver=new ChromeDriver();
		// Load URL
				driver.get("http://leaftaps.com/opentaps/");
				driver.manage().window().maximize();
				driver.findElementById("username").sendKeys("DemoSalesManager");
				driver.findElementById("password").sendKeys("crmsfa");
				driver.findElementByClassName("decorativeSubmit").click();
				driver.findElementByLinkText("CRM/SFA").click();
				driver.findElementByLinkText("Create Lead").click();
				driver.findElementByLinkText("Find Leads").click();
				driver.findElementByXPath("//span[text()='Email']").click();
				driver.findElementByName("emailAddress").sendKeys("revathynatarajan@gmail.com");
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				Thread.sleep(3000);
				String Leadid = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").getText();
				System.out.println(Leadid);
				String firstName=driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]").getText();
				String lastName=driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-lastName']/a[1]").getText();
				System.out.println(firstName);
				System.out.println(lastName);
				driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();
				driver.findElementByLinkText("Duplicate Lead").click();
				String title = driver.getTitle();
				if(title.equals("Duplicate Lead | opentaps CRM")) {
					System.out.println("The title name is Duplicte Lead");
					driver.findElementByClassName("smallSubmit").click();
					String fName = driver.findElementById("viewLead_firstName_sp").getText();
					String lName = driver.findElementById("viewLead_lastName_sp").getText();
					if(fName.equals(firstName)) {
						if(lName.equals(lastName)) {
							System.out.println("Duplicate name is same as captured name");
							
								
							}
						System.out.println("Name doesn't match");
						}
					
					}
					
					
				}
				

	}


