package week1.day4;

import org.openqa.selenium.chrome.ChromeDriver;

public class Verification {

	public static void main(String[] args) {
		
			//launch browser
			ChromeDriver driver=new ChromeDriver();
			//load URL
			driver.get("http://leaftaps.com/opentaps");
			//maximize
			driver.manage().window().maximize();
			//Enter username
			driver.findElementById("username").sendKeys("DemoSalesManager");
			//Enter password
			driver.findElementById("password").sendKeys("crmsfa");
			//click login
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Leads").click();
			driver.findElementByLinkText("Create Lead").click();
			
			String currentUrl = driver.getCurrentUrl();
			if(currentUrl.contains("createLeadForm")) {
				System.out.println(currentUrl);
				System.out.println("Pass");
								
			}

		String title = driver.getTitle();
		
		if(title.equals("Create Lead | opentaps CRM")) {
			System.out.println(title);
			System.out.println("correct title");
			
									
		}
		String text = driver.findElementById("createLeadForm_currencyUomId").getText();

		if (text.contains("USD")) {
			System.out.println("text contains USD");
			
			
		}
		
		boolean displayed = driver.findElementByClassName("smallSubmit").isDisplayed();
		System.out.println(displayed);
		boolean enabled = driver.findElementByClassName("smallSubmit").isEnabled();
		System.out.println(enabled);
		
		
	}

}
