package week1.day4;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class DeleteLeadHW extends ProjectSpecificMethods{
@Test
			public  void main()  throws InterruptedException {
			
			// Load URL
					WebElement lnk1 = locateElement("linkText","Create Lead");
					lnk1.click();
					WebElement lnk2 = locateElement("linkText","Find Leads");
					lnk2.click();
					WebElement lnk3 = locateElement("linkText","Phone");
					lnk3.click();
					WebElement ele1 = locateElement("xpath","//input[@name='phoneNumber']");
					type(ele1,"8056228722");
					WebElement btn1 = locateElement("xpath","//button[text()='Find Leads']");
					btn1.click();
					Thread.sleep(3000);
					WebElement ele2 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
					String Leadid = getText(ele2);
					System.out.println(Leadid);
					WebElement ele3 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
					ele3.click();
					Thread.sleep(3000);
					WebElement lnk4 = locateElement("linkText","Delete");
					lnk4.click();
					WebElement lnk5 = locateElement("linkText","Find Leads");
					lnk5.click();
					WebElement ele4 = locateElement("xpath","//input[@name='id']");
					type(ele4,Leadid);
					WebElement ele5 = locateElement("xpath","//button[text()='Find Leads']");
					ele5.click();
					Thread.sleep(3000);
					WebElement ele6 = locateElement("xpath","//div[text()='No records to display']");
					String errmsg = getText(ele6);
					
					if(errmsg.contains("No records to display")) {
						System.out.println("Deleted Successfully");
						
					}
					

	}

}
