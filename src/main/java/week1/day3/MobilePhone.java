package week1.day3;

public interface MobilePhone {
public int dialCaller(int phoneNo);
public boolean sendSMS(int phoneNo);
public void makeCall();
}
