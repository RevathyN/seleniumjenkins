package week1.day3;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectHW {

	public static void main(String[] args) {
		//launch browser
		ChromeDriver driver=new ChromeDriver();
		//load URL
		driver.get("http://leaftaps.com/opentaps");
		//maximize
		driver.manage().window().maximize();
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter password
		driver.findElementById("password").sendKeys("crmsfa");
		//click login
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("TestCompany");
		driver.findElementById("createLeadForm_firstName").sendKeys("First");
		driver.findElementById("createLeadForm_lastName").sendKeys("Last");
		WebElement sourceElements= driver.findElementById("createLeadForm_dataSourceId");
		Select sc=new Select(sourceElements);
		sc.selectByVisibleText("Partner");
		WebElement marketingEle = driver.findElementById("createLeadForm_marketingCampaignId");
		Select mar=new Select(marketingEle);
		mar.selectByValue("CATRQ_CARNDRIVER");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select ind=new Select(industry);
		List<WebElement> alloptions = ind.getOptions();
		int count=alloptions.size();
		System.out.println(count);
		
			WebElement lastOption = alloptions.get(count-1);
			ind.selectByIndex(count-1);
			
			System.out.println(lastOption.getText());
		}
		
		
		
		
		
				
		

	}


