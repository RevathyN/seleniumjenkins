package week2.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class MergeLeads extends ProjectSpecificMethods{
@Test
	public  void mergeLeads() throws InterruptedException {
		WebElement lnk = locateElement("linkText","Create Lead");
		lnk.click();
				WebElement lnk1 = locateElement("linkText","Merge Leads");
				lnk1.click();
		WebElement ele1 = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[1]");
		ele1.click();
				switchToWindow(1);
				WebElement fname = locateElement("xpath","//input[@name='firstName']");
				type(fname,"Revathy");
				WebElement btn = locateElement("xpath","//button[text()='Find Leads']");
				btn.click();
				Thread.sleep(3000);
				WebElement leadID = locateElement("xpath","(//a[@class='linktext'])[1]");
				String id1 = getText(leadID);
				WebElement ele2 = locateElement("xpath","(//a[@class='linktext'])[1]");
				ele2.click();
				switchToWindow(0);
				WebElement ele3 = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[2]");
				ele3.click();
				switchToWindow(1);
				WebElement fname2 = locateElement("name","firstName");
				type(fname2,"Revathy");
				WebElement btn2 = locateElement("xpath","//button[text()='Find Leads']");
				btn2.click();
				Thread.sleep(3000);
				WebElement ele4 = locateElement("xpath","(//a[@class='linktext'])[6]");
				ele4.click();
				switchToWindow(0);
				WebElement mrg = locateElement("linkText","Merge");
				mrg.click();
				acceptAlert();
				WebElement findlead = locateElement("linkText","Find Leads");
				findlead.click();
				WebElement id = locateElement("name","id");
				type(id,id1);
				WebElement flbtn = locateElement("xpath","//button[text()='Find Leads']");
				flbtn.click();
				Thread.sleep(3000);
				WebElement txt = locateElement("xpath","//div[text()='No records to display']");
				String text2 = getText(txt);
				verifyPartialText(txt,text2);
		/*Set<String> windowHandles = driver.getWindowHandles();
		List<String> lst=new ArrayList<String>();
		lst.addAll(windowHandles);
		driver.switchTo().window(lst.get(1));*/
		//driver.findElementByXPath("//input[@name='firstName']").sendKeys("Revathy");
		//driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		//String leadID = driver.findElementByXPath("(//a[@class='linktext'])[1]").getText();
		//driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		//driver.switchTo().window(lst.get(0));
		//driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		/*Set<String> windowHandles1 = driver.getWindowHandles();
		List<String> lst1=new ArrayList<String>();
		lst1.addAll(windowHandles1);
		driver.switchTo().window(lst1.get(1));*/
		//driver.findElementByName("firstName").sendKeys("Revathy");
		//driver.findElementByXPath("//button[text()='Find Leads']").click();
		

		//driver.findElementByXPath("(//a[@class='linktext'])[6]").click();
		//driver.switchTo().window(lst1.get(0));
		//driver.findElementByLinkText("Merge").click();
		//driver.switchTo().alert().accept();
		//driver.findElementByLinkText("Find Leads").click();
		//driver.findElementByName("id").sendKeys(leadID);
		//driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		//String text = driver.findElementByXPath("//div[text()='No records to display']").getText();
		//System.out.println(text);
		
	
		
		
		
		
		
		
		
		
		
		

	}

}
