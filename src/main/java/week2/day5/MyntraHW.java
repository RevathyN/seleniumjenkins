package week2.day5;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class MyntraHW {

	public static void main(String[] args) {
		RemoteWebDriver driver=new ChromeDriver();
		driver.get("https://www.myntra.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//driver.switchTo().alert().dismiss();
		driver.findElementByClassName("desktop-searchBar").sendKeys("Sunglasses");
		driver.findElementByClassName("desktop-submit").click();
		List<WebElement> totalele = driver.findElementsByClassName("product-price");
		System.out.println("Total sunglasses displayed is " + totalele.size());
		
		
		List<WebElement> unidisc = driver.findElementsByXPath("//div[@class='product-productMetaInfo']/h4[contains(text(),'Unisex')]/following::span[contains(text(),'(60% OFF)')]");
		System.out.println("Sunglasses with 60% off and unisex is " + unidisc.size());
		List<WebElement> brandName = driver.findElementsByXPath("//div[@class='product-productMetaInfo']/h4[contains(text(),'Unisex')]/following::span[contains(text(),'(60% OFF)')]/preceding::div[1]");
		for (WebElement item : brandName) {
			String	brand=item.getText();
			System.out.println("The brand name is " + brand);
			
		}

			
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		

	}

}
