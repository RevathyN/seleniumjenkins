package week2.day5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {

	public static void main(String[] args) {
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("//div[@class='items'][3]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		//Get the current Date 
		Date date = new Date(); 
		// Get only the date (and not month, year, time etc) 
		  DateFormat sdf = new SimpleDateFormat("dd");  
		// Get today's date 
		  String today = sdf.format(date); 
		// Convert to integer and add 1 to it 
		  int tomorrow = Integer.parseInt(today)+1; 
		// Print tomorrow's date 
		  System.out.println(tomorrow);
		
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		driver.findElementByXPath("//button[text()='Done']").click();
		List<WebElement> lst = driver.findElementsByXPath("//div[@class='component-car-item']");
	int size = lst.size();
		System.out.println("No. of items is " +size);
		List<WebElement> price = driver.findElementsByXPath("//div[@class='price']");
		List<Integer> allPrice=new ArrayList<Integer>();
		for (WebElement item : price) {
			String a = item.getText();
			String pr = a.replaceAll("\\D", "");
			int p=Integer.parseInt(pr);
			System.out.println(p);
			allPrice.add(p);
			
						
			
		}
	System.out.println("The max values is " +Collections.max(allPrice));
	
	int maxval=Collections.max(allPrice);
	String carName = driver.findElementByXPath("//div[contains(text(),'"+maxval+"')]/preceding::h3[1]").getText();
	System.out.println(carName);
	driver.findElementByXPath("//div[contains(text(),'"+maxval+"')]/following::button[1]").click();
	
	
	
			
		
		
		

	}

}










