package week2.day4;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class LearnActions {

	public static void main(String[] args) {
RemoteWebDriver driver=new ChromeDriver();
driver.get("http://jqueryui.com/draggable/");
driver.manage().window().maximize();
driver.switchTo().frame(0);
WebElement drag = driver.findElementById("draggable");
Actions builder=new Actions(driver);
//Point location = drag.getLocation().getY()
//builder.dragAndDrop(drag, location+30);
builder.dragAndDropBy(drag, 100, 100).perform();
//driver.findElementByLinkText("Selectable").click();


	}

}
