package week2.day4;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ActionsSelectable {

	public static void main(String[] args) {
		RemoteWebDriver driver=new ChromeDriver();
		driver.get("http://jqueryui.com/selectable/");
		driver.switchTo().frame(0);
		WebElement ele1 = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement ele2 = driver.findElementByXPath("//li[text()='Item 4']");
		WebElement ele3 = driver.findElementByXPath("//li[text()='Item 7']");

		Actions builder=new Actions(driver);
		builder.keyDown(Keys.CONTROL).perform();
		builder.click(ele1).perform();
		builder.click(ele2).perform();
		builder.click(ele3).perform();
		
		
		
		

	}

}
