package week2.day3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListDuplicatesRemove {

	public static void main(String[] args) {
		List<String> lst=new ArrayList<String>();
		lst.add("Revathy");
		lst.add("Priya");
		lst.add("Preethi");
		lst.add("Revathy");
		lst.add("Deepthi");
		
		Set<String> set=new HashSet<String>();
		set.addAll(lst);
		System.out.println(set.size());
		for (String string : set) {
			System.out.println(string);
		}

	}

}
