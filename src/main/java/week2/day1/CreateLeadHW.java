package week2.day1;
import org.openqa.selenium.chrome.ChromeDriver;


public class CreateLeadHW {
	
public  void main() {
ChromeDriver driver=new ChromeDriver();
// Load URL
driver.get("http://leaftaps.com/opentaps/");
driver.manage().window().maximize();
driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();
driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Create Lead").click();
driver.findElementById("createLeadForm_companyName").sendKeys("Revathy");
driver.findElementById("createLeadForm_firstName").sendKeys("First");
driver.findElementById("createLeadForm_lastName").sendKeys("Last");
driver.findElementById("createLeadForm_dataSourceId").sendKeys("CONFERENCE");
driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Car and Driver");
driver.findElementById("createLeadForm_firstNameLocal").sendKeys("LocalFirstName");
driver.findElementById("createLeadForm_lastNameLocal").sendKeys("LocalLastName");
driver.findElementById("createLeadForm_personalTitle").sendKeys("Ms");
driver.findElementById("createLeadForm_generalProfTitle").sendKeys("DR");
driver.findElementById("createLeadForm_departmentName").sendKeys("Test Dept");
driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee");
driver.findElementById("createLeadForm_industryEnumId").sendKeys("Computer Software");
driver.findElementById("createLeadForm_numberEmployees").sendKeys("3000");
driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("S-Corporation");
driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8056228722");
driver.findElementById("createLeadForm_primaryEmail").sendKeys("revathynatarajan@gmail.com");
driver.findElementByClassName("smallSubmit").click();
driver.findElementByLinkText("Logout").click();






	}

}
