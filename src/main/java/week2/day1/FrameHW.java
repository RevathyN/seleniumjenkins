package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class FrameHW {

	public static void main(String[] args) {
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://layout.jquery-dev.com/demos/iframe_local.html");
		driver.manage().window().maximize();
		driver.switchTo().frame("childIframe");
		driver.findElementByXPath("//button[text()='Close Me'][1]").click();
		System.out.println("Iframe West CloseMe clicked");
		driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
		System.out.println("Iframe East CloseMe clicked");
		
				driver.switchTo().defaultContent();
		driver.findElementByXPath("//button[text()='Close Me'][1]").click();
		System.out.println("West CloseMe clicked");
driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
System.out.println("East CloseMe clicked");
		
		driver.close();
		

	}

}
